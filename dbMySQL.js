var mysql =require('mysql');
var connection= mysql.createConnection({
	host:'localhost',
	user:'root',
	password:'Parool11',
	database:'haldusprogramm'
});

connection.connect();


// **********   ETTEVÕTTED   ********* //

exports.addNewCompany = function(newCompanyName,newCompanyTel,newCompanyEmail,newCompanyContact,newCompanyAddress, callback){
    connection.query("INSERT INTO ettevotted (nimi,tel,email,kontaktisik,aadress) VALUES ('" + newCompanyName + "','" + newCompanyTel +
        "','" + newCompanyEmail + "','"+ newCompanyContact+"','" + newCompanyAddress+  "')", function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing1 query');
        }
    });
};

exports.getAllAmetid = function(callback) {
    connection.query('SELECT * FROM ametid', function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing2 query');
        }
    });
};

exports.getAllCompany = function(callback) {
    connection.query('SELECT * FROM ettevotted', function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing3 query');
        }
    });
};

exports.deleteCompanyById = function(id, callback) {
    connection.query('DELETE FROM ettevotted WHERE id = ' +id, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing4  query');
        }
    });
};

exports.updateCompany = function( modifiedCompany,callback) {
    connection.query("UPDATE ettevotted SET nimi = '" + modifiedCompany.nimi +"', kontaktisik = '"
        + modifiedCompany.kontaktisik + "', aadress = '" + modifiedCompany.aadress + "', tel = '"
        + modifiedCompany.tel + "', email = '" + modifiedCompany.email +  "' WHERE id = "
        + modifiedCompany.id, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing5 query');
        }
    });
};


// **********   TÖÖTAJAD   ********* //

exports.addNewWorker = function(newWorkerEesnimi,newWorkerPerekonnanimi,newWorkerAmet,newWorkerEttevotte_id,newWorkerTelefon,newWorkerEmail, callback){
    console.log("INSERT INTO tootajad (eesnimi,perekonnanimi,ameti_id,ettevotte_id,tel,email) VALUES ('" + newWorkerEesnimi + "','" + newWorkerPerekonnanimi +"','" + newWorkerAmet + "','"+ newWorkerEttevotte_id +"','"+ newWorkerTelefon +"','"+ newWorkerEmail +"')");
	connection.query("INSERT INTO tootajad (eesnimi,perekonnanimi,ameti_id,ettevotte_id,tel,email) VALUES ('" + newWorkerEesnimi + "','" + newWorkerPerekonnanimi +"','" + newWorkerAmet + "','"+ newWorkerEttevotte_id +"','"+ newWorkerTelefon +"','"+ newWorkerEmail +"')", function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on  performing add query');
        }
    });
}

exports.getAllWorker = function(callback) {
    connection.query('SELECT tootajad.*, ametid.amet, ettevotted.nimi FROM ametid INNER JOIN tootajad ON  ametid.id = tootajad.ameti_id INNER JOIN ettevotted ON  ettevotted.id = tootajad.ettevotte_id', function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing7 query');
        }
    });
};

exports.deleteWorkerById = function(id, callback) {
    connection.query('DELETE FROM tootajad WHERE id = ' +id, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing8 delete query');
        }
    });
};

exports.updateWorker = function( modifiedWorker,callback) {
	connection.query("UPDATE tootajad SET eesnimi = '" + modifiedWorker.eesnimi +"', perekonnanimi = '"+ modifiedWorker.perekonnanimi + 
	"', ameti_id = (SELECT id from ametid WHERE amet = '" + modifiedWorker.ameti_id + "', ettevotte_id =(SELECT id from ettevotted WHERE nimi =  '" + modifiedWorker.ettevotte_id + "', tel = '" 
	+ modifiedWorker.tel + "', email = '" + modifiedWorker.email + "' WHERE id = " + modifiedWorker.id, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on UPDATEperforming query');
        }
    });
};


// **********   KOOLITUSED   ********* //

exports.addNewKoolitus = function(newKoolitusNimi,newKoolitusKirjeldus,newKoolitusTase,newKoolitusKoolitustasu,newKoolitusKestus, callback){
	console.log ("INSERT INTO koolitused (nimi,kirjeldus,tase,koolitustasu,kestus) VALUES ('" + newKoolitusNimi + "','" + newKoolitusKirjeldus +"','" 
        + newKoolitusTase + "','"+ newKoolitusKoolitustasu +"','" + newKoolitusKestus + "')");
    connection.query("INSERT INTO koolitused (nimi,kirjeldus,tase,koolitustasu,kestus) VALUES ('" + newKoolitusNimi + "','" + newKoolitusKirjeldus +"','" 
        + newKoolitusTase + "','"+ newKoolitusKoolitustasu +"','" + newKoolitusKestus + "')", function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing10 query');
        }
    });
}
exports.getAllKoolitus = function(callback) {
    connection.query('SELECT * FROM koolitused', function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing11 query');
        }
    });
};
exports.deleteKoolitusById = function(id, callback) {
    connection.query('DELETE FROM koolitused WHERE id = ' +id, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing12 query');
        }
    });
};
exports.saveKoolitus = function( modifiedKoolitus,callback) {
    connection.query("UPDATE koolitused SET nimi = '" + modifiedKoolitus.nimi +"', kirjeldus = '"+ modifiedKoolitus.kirjeldus + "', tase = '" + modifiedKoolitus.tase + "', koolitustasu = '" + modifiedKoolitus.koolitustasu + "', kestus = '" + modifiedKoolitus.kestus+ "' WHERE id = " + modifiedKoolitus.id, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing13 query');
        }
    });
};


// **********   RIISTVARA + TARKVARA   ********* //

/* exports.addNewCompany = function(newCompanyName,newCompanyTel,newCompanyEmail,newCompanyContact,newCompanyAddress, callback){
    connection.query("INSERT INTO ettevotted (nimi,tel,email,kontaktisik,aadress) VALUES ('" + newCompanyName + "','" + newCompanyTel +
        "','" + newCompanyEmail + "','"+ newCompanyContact+"','" + newCompanyAddress+  "')", function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing14 query');
        }
    });
}; */


exports.getAllRiistvara = function(callback) {
    connection.query('SELECT * FROM riistvara', function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing15 query');
        }
    });
};
exports.getAllRecRiistvara = function(callback) {
    connection.query('SELECT * FROM riistvara', function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing16 query');
        }
    });
};
exports.getAllTarkvara = function(callback) {
    connection.query('SELECT * FROM tarkvara', function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing17 query');
        }
    });
	
};exports.getAllRecTarkvara = function(callback) {
    connection.query('SELECT * FROM tarkvara', function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing18 query');
        }
    });
};
exports.addNewRiistvara = function(newArvuti,newPrinter,newMonitor,newHiir,newKlaviatuur,newRuuter,newKolarid,newKlapid,newProjektor, callback){
    connection.query("INSERT INTO riistvara (arvuti_nimetus,printeri_nimetus,monitori_nimetus,hiire_nimetus,klaviatuuri_nimetus,ruuteri_nimetus,kolari_nimetus,korvaklappide_nimetus,projektori_nimetus) VALUES ('" + newArvuti + "','" + newPrinter +"','" + newMonitor + "','"+ newHiir +"','"+ newKlaviatuur +"','"+ newRuuter +"','"+ newKolarid +"','"+ newKlapid +"','"+ newProjektor +"')", function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing19 query');
        }
    });
}
exports.addNewTarkvara = function(newOpsystem,newMsoffice,newAntiviirus, callback){
    connection.query("INSERT INTO tarkvara (operatsioonisysteem,ms_office_versioon,antiviirus) VALUES ('" +newOpsystem+ "','"+newMsoffice+"','" 
        +newAntiviirus+ "')", function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing20 query');
        }
    });
}

/*
exports.deleteCompanyById = function(id, callback) {
    connection.query('DELETE FROM ettevotted WHERE id = ' +id, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing21 query');
        }
    });
};

exports.updateCompany = function( modifiedCompany,callback) {
    connection.query("UPDATE ettevotted SET nimi = '" + modifiedCompany.nimi +"', kontaktisik = '"
        + modifiedCompany.kontaktisik + "', aadress = '" + modifiedCompany.aadress + "', tel = '"
        + modifiedCompany.tel + "', email = '" + modifiedCompany.email +  "' WHERE id = "
        + modifiedCompany.id, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing22 update query');
        }
    });
};
*/
