var koolitused;
var koolitus;
function fillAllKoolitusTable() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/koolitused",
        success: function(koolitusResponse){
            koolitus = koolitusResponse;
            var tableContent = "";

            for(var i = 0; i < koolitusResponse.length; i++) {
                tableContent = tableContent + '</tr><tr><th>'+ (i+1) +' </th><td id="koolitusnimiID'
					+ koolitusResponse[i].id+'">'
                    + koolitusResponse[i].nimi + '</td><td id="kirjeldusID'
					+ koolitusResponse[i].id+'">'
					+ koolitusResponse[i].kirjeldus + '</td><td id="taseID'
					+ koolitusResponse[i].id+'">' 
					+ koolitusResponse[i].tase + '</td><td id="koolitustasuID'
					+ koolitusResponse[i].id+'">'
					+ koolitusResponse[i].koolitustasu +'</td><td  id="kestusID'+ koolitusResponse[i].id+'">' 
					+ koolitusResponse[i].kestus
                    + '</td>  <th> <button id="' + koolitusResponse[i].id + '" class="editbtn btn btn-default">Muuda</button> <button class="btn btn-default modify-koolitus" data-toggle="modal" data-target="#deleteKoolitusModal"   onClick ="openDeletekoolitusConfirmModal(' + koolitusResponse[i].id
                    + ')">Kustuta</button></th></tr>';
            }
            document.getElementById("koolitusTable").innerHTML = tableContent;
        }
    });
}



function addNewKoolitus() {
    var newName = document.getElementById("newKoolitusNimi").value;
    var newDescription = document.getElementById("newKoolitusKirjeldus").value;
    var newLevel = document.getElementById("newKoolitusTase").value;
    var newCost = document.getElementById("newKoolitusKoolitustasu").value;
    var newDuration = document.getElementById("newKoolitusKestus").value;
   if((newName != "")&& (newDescription != "")&& (newLevel != "")&& (newCost != "")&& (newDuration != "")){
        var newKoolitus = JSON.stringify({"nimi": newName,"kirjeldus": newDescription,"tase": newLevel,"koolitustasu": newCost ,"kestus": newDuration});

        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: newKoolitus,
            url: "http://localhost:3000/koolitused",
            success: function(koolitusResponse){
                fillAllKoolitusTable();
                showAlert("success-alert-koolitused");
                location.reload();
            }
         });
    }else{
        $("#danger-alert-koolitused").alert();
        $("#danger-alert-koolitused").fadeTo(2000, 500);
    }
	
}

function showAlert(alertId){
    $("#"+alertId).alert();
    $("#"+alertId).fadeTo(2000, 500).slideUp(500, function(){
        $("#"+alertId).slideUp(500);
    });
}

function saveKoolitus(id){
	
	var inputValue = document.getElementById("koolitusnimiID"+id).innerHTML;
    var inputValue1 = document.getElementById("kirjeldusID"+id).innerHTML;
    var inputValue2 = document.getElementById("taseID"+id).innerHTML;
    var inputValue3 = document.getElementById("koolitustasuID"+id).innerHTML;
    var inputValue4 = document.getElementById("kestusID"+id).innerHTML;
    var modifiedKoolitus = JSON.stringify({"id": id,"nimi": inputValue, "kirjeldus": inputValue1, "tase":inputValue2, "koolitustasu":inputValue3, "kestus":inputValue4});

        $.ajax({
            type: "PUT",
            dataType: "json",
            contentType: "application/json",
            data: modifiedKoolitus,
            url: "http://localhost:3000/koolitused",
            success: function(koolitusResponse){
                fillAllKoolitusTable();
            }
        });
		
	}

var koolitusToDeleteId;
var koolitusToDeleteName;
var koolitusToDeleteDescription;
var koolitusToDeleteLevel;
var koolitusToDeleteCost;
var koolitusToDeleteDuration;
function openDeletekoolitusConfirmModal(selectedKoolitusId){
    for(var i= 0; i<koolitus.length; i++){
        if(koolitus[i].id == selectedKoolitusId){
            koolitusToDeleteId = selectedKoolitusId;
            koolitusToDeleteName = koolitus[i].nimi;
			koolitusToDeleteDescription = koolitus[i].kirjeldus;
			koolitusToDeleteLevel = koolitus[i].tase;
			koolitusToDeleteCost = koolitus[i].koolitustasu;
            koolitusToDeleteDuration = koolitus[i].kestus
            $(".modal-body #deleteMessage").text("Kas oled kindel, et soovid kustutada koolituse " + koolitusToDeleteName + "?");
			}
		}
	}

function deletekoolitus() {
   if(koolitusToDeleteId != null){
        $.ajax({
            type: "DELETE",
            dataType: "json",
            contentType: "application/json",
            url: "http://localhost:3000/koolitused/" + koolitusToDeleteId,
            success: function(koolitusResponse){
                fillAllKoolitusTable();
                $('#deleteKoolitusModal').modal('hide');
                showAlert("delete-success-alert-koolitused");
            }

        });
    }
    location.reload()
}

function SwitchButtons(buttonId) {
  var hideBtn, showBtn;
  if (buttonId == 'button1') {
    showBtn = 'button2';
    hideBtn = 'button1';
	function hiden(){
	document.getElementById(hideone).style.display = 'none';
	}
	return hiden();
  } else {
    showBtn = 'button1';
    hideBtn = 'button2';
}

document.getElementById(hideBtn).style.display = 'none';
document.getElementById(showBtn).style.display = '';
}

function Hideable() {
    var x = document.getElementById('Hideable');
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
