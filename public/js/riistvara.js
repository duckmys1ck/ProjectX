var ettevotted;
var riistvara;
var tarkvara;
var ametid;

function fillAllRiistvaraTable() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/riistvara",
        success: function(riistvaraResponse){
            riistvara = riistvaraResponse;
		var nimetus = ["#", "Arvutid:", "Printerid:","Monitorid:","Hiired:","Klaviatuurid:","Ruuterid:","Kõlarid:","Kõrvaklapid:","Projektor:"];
		var tableContents = "";
		
for(var i = 0; i < Object.keys(riistvara[0]).length; i++) {
    tableContents =  tableContents + "<tr><th>" + nimetus[i] + "</th>";

    for(var j = 1; j < riistvara.length; j++) {
		
		if (riistvara[j][(Object.keys(riistvara[0])[i])] != null) {
			if(i==0){
				tableContents =  tableContents + '<td><button class="btn btn-default modify-riistvara" data-toggle="modal" data-target="#deleteRiistvaraModal"   onClick ="openDeleteRiistvaraConfirmModal('+ riistvaraResponse[i].id +')">Kustuta</button></td>';
			}else {
				tableContents =  tableContents + "<td>" + riistvara[j][(Object.keys(riistvara[0])[i])] + "</td>";
			}
		}else{
			tableContents=tableContents+"<td></td>";
		}
	}

    tableContents =  tableContents + "</tr>";
}


                  document.getElementById("RiistvaraTable").innerHTML = tableContents;

        }
    });
}
function RecRiistvaraTable(){
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/recommendedriistvara",
        success: function(riistvaraResponse){
            riistvara = riistvaraResponse;
		var nimetus = ["#", "Arvutid:", "Printerid:","Monitorid:","Hiired:","Klaviatuurid:","Ruuterid:","Kõlarid:","Kõrvaklapid:","Projektor:"];
		var tableContents = "";
for(var i = 0; i < Object.keys(riistvara[0]).length; i++) {
    tableContents = tableContents + "<tr><th>" + nimetus[i] + "</th>";



		if (riistvara[0][(Object.keys(riistvara[0])[i])] != null) {
			if(i==0){
				tableContents =  tableContents + '<td><button class="btn btn-default modify-riistvara" data-toggle="modal" data-target="#deleteRiistvaraModal"  >Kustuta</button></td>';
			}else {
				tableContents =  tableContents + "<td>" + riistvara[0][(Object.keys(riistvara[0])[i])] + "</td>";
			}
		}else{
			tableContents=tableContents+"<td></td>";
		}

    tableContents = tableContents + "</tr>";
}


                  document.getElementById("RecRiistvaraTable").innerHTML = tableContents;

        }
    });
}

function fillAllTarkvaraTable() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/tarkvara",
        success: function(tarkvaraResponse){
            tarkvara = tarkvaraResponse;
            var tableContent = "";

		var nimetus = ["#", "Op. Süsteem:", "MS Office:","Antiviirus:"];
		var tableContents = "";
for(var i = 0; i < Object.keys(tarkvara[0]).length; i++) {
    tableContents = tableContents + "<tr><th>" + nimetus[i] + "</th>";

    for(var j = 1; j < tarkvara.length; j++) {
	if (tarkvara[j][(Object.keys(tarkvara[0])[i])] != null) {
			if(i==0){
				tableContents =  tableContents + '<td><button class="btn btn-default modify-riistvara" data-toggle="modal" data-target="#deleteRiistvarayModal"   onClick ="openDeleteTarkvaraConfirmModal('+ tarkvaraResponse[i].id+ ')">Kustuta</button></td>';
			}else {
				tableContents =  tableContents + "<td>" + tarkvara[j][(Object.keys(tarkvara[0])[i])] + "</td>";
			}
		}else{
			tableContents=tableContents+"<td></td>";
		}
	}
	
    tableContents = tableContents + "</tr>";
}


                  document.getElementById("TarkvaraTable").innerHTML = tableContents;
        }
    });
}
function RecTarkvaraTable() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/recommendedtark",
        success: function(tarkvaraResponse){
            tarkvara = tarkvaraResponse;
            var tableContent = "";

		var nimetus = ["#", "Op. Süsteem:", "MS Office:","Antiviirus:"];
		var tableContents = "";
for(var i = 0; i < Object.keys(tarkvara[0]).length; i++) {
    tableContents = tableContents + "<tr><th>" + nimetus[i] + "</th>";

		if (tarkvara[0][(Object.keys(tarkvara[0])[i])] != null) {
			if(i==0){
				tableContents =  tableContents + '<td><button class="btn btn-default modify-riistvara" data-toggle="modal" data-target="#deleteRiistvaraModal"   onClick ="openDeleteTarkvaraConfirmModal('+ tarkvaraResponse[i].id+ ')">Kustuta</button></td>';
			}else {
				tableContents =  tableContents + "<td>" + tarkvara[0][(Object.keys(tarkvara[0])[i])] + "</td>";
			}
		}else{
			tableContents=tableContents+"<td></td>";
		}
    tableContents = tableContents + "</tr>";
}


                  document.getElementById("RecTarkvaraTable").innerHTML = tableContents;
        }
    });
}


function addRiistvara(){
    var inputArvuti = document.getElementById("newArvuti").value;
    var inputPrinter = document.getElementById("newPrinter").value;
    var inputMonitor = document.getElementById("newMonitor").value;
    var inputHiir = document.getElementById("newHiir").value;
    var inputKlaviatuur = document.getElementById("newKlaviatuur").value;
	var inputRuuter = document.getElementById("newRuuter").value;
    var inputKolarid = document.getElementById("newKolarid").value;
    var inputKlapid = document.getElementById("newKlapid").value;
	var inputProjektor = document.getElementById("newProjektor").value;



        var newRiistvara = JSON.stringify({"arvuti_nimetus": inputArvuti,"printeri_nimetus": inputPrinter,"monitori_nimetus": inputMonitor,"hiire_nimetus": inputHiir,"klaviatuuri_nimetus": inputKlaviatuur,"ruuteri_nimetus": inputRuuter,"kolari_nimetus": inputKolarid,"korvaklappide_nimetus": inputKlapid,"projektori_nimetus": inputProjektor});
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: newRiistvara,
            url: "http://localhost:3000/riistvara",
            success: function(riistvaraResponse){
                fillAllRiistvaraTable();
                showAlert("success-alert");
                location.reload();
            }
         });

}
function addNewTarkvara(){
	var inputOpsystem = document.getElementById("newOpsystem").value;
	var inputMsoffice = document.getElementById("newMsoffice").value;
	var inputAntiviirus = document.getElementById("newAntiviirus").value;
	
		var newTarkvara = JSON.stringify({"operatsioonisysteem":inputOpsystem,"ms_office_versioon":inputMsoffice,"antiviirus":inputAntiviirus});
		$.ajax({
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: newTarkvara,
            url: "http://localhost:3000/tarkvara",
            success: function(riistvaraResponse){
                fillAllRiistvaraTable();
                showAlert("success-alert");
                location.reload();
            }
         });
}

function showAlert(alertId){
    $("#"+alertId).alert();
    $("#"+alertId).fadeTo(2000, 500).slideUp(500, function(){
        $("#"+alertId).slideUp(500);
    });
}




var riistvaraToDeleteId;
var riistvaraToDeleteArvuti;
var riistvaraToDeletePrinter;
var riistvaraToDeleteMonitor;
var riistvaraToDeleteHiir;
var riistvaraToDeleteKlaviatuur;
var riistvaraToDeleteRuuter;
var riistvaraToDeleteKolarid;
var riistvaraToDeleteKlapid;
var riistvaraToDeleteProjektor;
function openDeleteRiistvaraConfirmModal(selectedRiistvaraId){
    for(var i= 0; i<riistvara.length; i++){
        if(riistvara[i].id == selectedRiistvaraId){
            riistvaraToDeleteId = selectedRiistvaraId;
            riistvaraToDeleteArvuti = riistvara[i].arvuti_nimetus;
			riistvaraToDeletePrinter = riistvara[i].printeri_nimetus;
			riistvaraToDeleteMonitor = riistvara[i].monitori_nimetus;
			riistvaraToDeleteHiir = riistvara[i].hiire_nimetus;
			riistvaraToDeleteKlaviatuur = riistvara[i].klaviatuuri_nimetus;
            riistvaraToDeleteRuuter = riistvara[i].ruuteri_nimetus;
			riistvaraToDeleteKolarid = riistvara[i].kolari_nimetus;
			riistvaraToDeleteKlapid = riistvara[i].korvaklappide_nimetus;
			riistvaraToDeleteProjektor = riistvara[i].projektori_nimetus;
            $(".modal-body #deleteMessage").text("Kas oled kindel, et soovid kustutada riistvara " + riistvaraToDeleteId+ "?");
        }
    }
}

function deleteRiistvara() {
   if(riistvaraToDeleteId != null){
        $.ajax({
            type: "DELETE",
            dataType: "json",
            contentType: "application/json",
            url: "http://localhost:3000/riistvara/" + riistvaraToDeleteId,
            success: function(riistvaraResponse){
                fillAllRiistvaraTable();
                $('#deleteRiistvaraModal').modal('hide');
                showAlert("delete-success-alert");
            }

        });
    }
    location.reload()
}
/*

function SwitchButtons(buttonId) {
	  var hideBtn, showBtn, menuToggle;
	  if (buttonId == 'button1') {
		menuToggle = 'menu2';
		showBtn = 'button2';
		hideBtn = 'button1';
	  } else {
		menuToggle = 'menu3';
		showBtn = 'button1';
		hideBtn = 'button2';
	  }
	  document.getElementById(hideBtn).style.display = 'none';
	  document.getElementById(showBtn).style.display = '';


}*/
function Hideable() {
    var peidaelement = document.getElementsByClassName('Hideable');
    if (peidaelement.style.display === "none") {
        peidaelement.style.display = "block";
    } else {
        peidaelement.style.display = "none";
    }
}
