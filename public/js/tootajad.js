var tootajad;
var worker;
function fillAllWorkerTable() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/tootajad",
        success: function(workerResponse){
            worker = workerResponse;
            var tableContent = "";
            for(var i = 0; i < workerResponse.length; i++) {
                tableContent = tableContent + '</tr><tr><td>'+ (i+1) +' </td><td id="nimi'+workerResponse[i].id+'">'
                    + workerResponse[i].eesnimi + '</td><td id="perekonnanimi'+workerResponse[i].id+'">' 
					+ workerResponse[i].perekonnanimi + '</td><th id="ameti_id'+workerResponse[i].id+'">' 
                    + workerResponse[i].amet	 +'</th><th id="ettevotte_id'+workerResponse[i].id+'">' 
					+ workerResponse[i].nimi + '</th><td id="telefon'
					+ workerResponse[i].id+'">' 
                    + phoneFormatter("" + workerResponse[i].tel) +'</td><td id="email'+ workerResponse[i].id+'">' 
					+ workerResponse[i].email + ' </td><td><button id="' + workerResponse[i].id + 
                    '" class="editbtn btn btn-default">Muuda</button> <button class="btn btn-default modify-worker" data-toggle="modal" data-target="#deleteWorkerModal" onClick ="openDeleteWorkerConfirmModal(' + workerResponse[i].id + ')">Kustuta</button></td></tr>';
            }

            document.getElementById("WorkerTable").innerHTML = tableContent;
        }
    });
}
function intOnly(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && charCode != 43  && (charCode < 43 || charCode > 57)) {
        return false;	
    }else
    return true;
}
function phoneFormatter(numberToFormat){
	if(numberToFormat.length<=6){
		return numberToFormat.replace(/(\d\d\d)(\d\d\d)/, '$1-$2');
	}else if (numberToFormat.length<=7){
		return numberToFormat.replace(/(\d\d\d)(\d\d\d)(\d)/, '$1-$2-$3');
	}else if(numberToFormat.length<=8){
		return numberToFormat.replace(/(\d\d\d)(\d\d\d)(\d\d)/, '$1-$2-$3');
	}else if(numberToFormat.length<=9){
		return numberToFormat.replace(/(\d\d\d)(\d\d\d)(\d\d\d)/, '$1-$2-$3');
	}else{
		return numberToFormat.replace(/(\d\d\d)(\d\d\d)(\d\d\d)/, '$1-$2-$3');
	}
};

function fillAllAmetid() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/ametid",
        success: function(ametidResponse){
            ametid = ametidResponse;
            var optionContent = "";

			for(var i = 0; i< ametidResponse.length; i++){
				optionContent = optionContent + '<option value="'+ametidResponse[i].id+'">'+ametidResponse[i].amet+'</option>';
			}
			document.getElementById("newWorkerAmet").innerHTML = optionContent;
		}
	});
}
function fillAllEttevotted() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/ettevotted",
        success: function(ettevottedResponse){
            Ettevotted = ettevottedResponse;
            var optionContent = "";

			for(var i = 0; i< ettevottedResponse.length; i++){
				optionContent = optionContent + '<option value="'+ettevottedResponse[i].id+'">'+ettevottedResponse[i].nimi+'</option>';
			}
			document.getElementById("newWorkerEttevotte_id").innerHTML = optionContent;
		}
	});
}

function addNewWorker(){
    var inputValue = document.getElementById("newWorkerEesnimi").value;
    var inputValue1 = document.getElementById("newWorkerPerekonnanimi").value;
    var inputValue2 = document.getElementById("newWorkerAmet").value;
    var inputValue3 = document.getElementById("newWorkerEttevotte_id").value;
    var inputValue4 = document.getElementById("newWorkerTelefon").value;
	var inputValue5 = document.getElementById("newWorkerEmail").value;
        var newWorker = JSON.stringify({"eesnimi": inputValue,"perekonnanimi": inputValue1,"ameti_id": inputValue2,"ettevotte_id": inputValue3,"tel": inputValue4,"email": inputValue5});
        console.log(newWorker);
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: newWorker,
            url: "http://localhost:3000/tootajad",
            success: function(workerResponse){
                console.log(workerResponse);
                fillAllWorkerTable();
                showAlert("success-alert-tootajad");
                location.reload();
            }
         });

}

function showAlert(alertId){
	$("#"+alertId).alert();
	$("#"+alertId).fadeTo(2000, 500).slideUp(500, function(){
		$("#"+alertId).slideUp(500);
	});
}

function saveWorker(id){
 console.log(id);
  var inputValue = document.getElementById("nimi"+id).innerHTML;
  var inputValue1 = document.getElementById("perekonnanimi"+id).innerHTML;
  var inputValue2 = document.getElementById("ameti_id"+id).value;
  var inputValue3 = document.getElementById("ettevotte_id"+id).value;
  var inputValue4 = document.getElementById("telefon"+id).innerHTML;
  var inputValue5 = document.getElementById("email"+id).innerHTML;
  var modifiedWorker = JSON.stringify({"id": id,"eesnimi": inputValue,"perekonnanimi": inputValue1, "ameti_id": inputValue2, "ettevotte_id":inputValue3, "tel":inputValue4, "email":inputValue5 });
  console.log(modifiedWorker);
  $.ajax({
	  type: "PUT",
	  dataType: "json",
	  contentType: "application/json",
	  data: modifiedWorker,
	  url: "http://localhost:3000/tootajad",
	  success: function(workerResponse){
		  fillAllWorkerTable();
	  }
  });
}

var workerToDeleteId;
var workerToDeleteEesnimi;
var workerToDeletePerekonnanimi;
var workerToDeleteAmet;
var workerToDeleteEttevotteId;
var workerToDeleteTelefon;
var workerToDeleteEmail;
function openDeleteWorkerConfirmModal(selectedWorkerId){
    for(var i= 0; i<worker.length; i++){
        if(worker[i].id == selectedWorkerId){
			workerToDeleteId = selectedWorkerId;
			workerToDeleteEesnimi = worker[i].eesnimi;
			workerToDeletePerekonnanimi = worker[i].perekonnanimi;
			workerToDeleteAmet = worker[i].ameti_id;
			workerToDeleteWorkerEttevotteId = worker[i].vanus;
			workerToDeleteWorkerTel= worker[i].tel;
			workerToDeleteWorkerEmail = worker[i].email;
            $(".modal-body #deleteMessage").text("Kas oled kindel, et soovid kustutada töötaja " + workerToDeleteEesnimi + " " + 
				workerToDeletePerekonnanimi + "?");
        }
    }
}

function deleteWorker() {
   if(workerToDeleteId != null){
        $.ajax({
			async: true,
            type: "DELETE",
            dataType: "json",
            contentType: "application/json",
            url: "http://localhost:3000/tootajad/" + workerToDeleteId,
            success: function(workerResponse){
                fillAllWorkerTable();
                $('#deleteWorkerModal').modal('hide');
                showAlert("delete-success-alert-tootajad");
            }

        });
    }
    location.reload()
}

function SwitchButtons(buttonId) {
  var hideBtn, showBtn;
  if (buttonId == 'button1') {
    showBtn = 'button2';
    hideBtn = 'button1';
	function hiden(){
	document.getElementById(hideone).style.display = 'none';
	}
	return hiden();
  } else {
    showBtn = 'button1';
    hideBtn = 'button2';
}

document.getElementById(hideBtn).style.display = 'none';
document.getElementById(showBtn).style.display = '';
}

function Hideable() {
    var x = document.getElementById('Hideable');
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

function addMassFile() {
    var file = document.getElementById("massFile").files[0];
    var reader = new FileReader();
    reader.readAsText(file, 'ISO-8859-4');
    reader.onload = loaded;
    reader.onerror = errorHandler;
	location.reload()
}

function loaded(event) {
    var fileString = event.target.result;
    console.log(fileString);
    if(fileString != "") {
      $.ajax({
       type: "POST",
       dataType: "json",
       contentType: "application/json",
       url: "http://localhost:3000/tootajadFile",
       data: JSON.stringify({"message":fileString}),
       success: function(response) {
         console.log(response);
       }
    });
  }
}

function errorHandler(event) {
  console.log("error occured!");
}

function processData(allText) {
    var allTextLines = allText.split(/\r\n|\n/);
    var headers = allTextLines[0].split(',');
    var lines = [];

    for (var i=1; i<allTextLines.length; i++) {
        var data = allTextLines[i].split(',');
        if (data.length == headers.length) {

          var tarr = [];
          for (var j=0; j<headers.length; j++) {
              tarr.push(headers[j]+":"+data[j]);
          }
          lines.push(tarr);
      }
    }
    for (var i=1; i<lines.length; i++) {
      var firstName = lines[i][0];
      var lastName = lines[i][1];
      var newWorkerId = lines[i][2];
      var newWorkerEttevotte_id = lines[i][3];
      var newPhone = lines[i][4];
	  var newEmail = lines[i][5];
      var addEmployee = JSON.stringify({"eesnimi": firstName, "perekonnanimi": lastName, "ameti_id": newWorkerId, "ettevotte_id": newWorkerEttevotte_id, "tel": newPhone, "email":newEmail});
      console.log(addEmployee);
      $.ajax({
          type: "POST",
          dataType: "json",
          contentType: "application/json",
          data: addEmployee,
          url: "http://localhost:3000/tootajad",
          success: function(employeesResponse){
              console.log(employeesResponse);
              fillAllEmployeesTable();
          }
      });
  }

console.log(lines);
}