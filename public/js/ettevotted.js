var ettevotted;
var company;
var ametid;
function fillAllCompanyTable() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/ettevotted",
        success: function(companyResponse){
            company = companyResponse;
            var tableContent = "";
            for(var i = 0; i < companyResponse.length; i++) {
                tableContent = tableContent + '</tr><tr><th>'+ (i+1) +' </th><td id="firmaid'+companyResponse[i].id+'">'
                    + companyResponse[i].nimi +'</td><td id="isikuid'+companyResponse[i].id+'">'
					+ companyResponse[i].kontaktisik + '</td><td id="aadressid'+companyResponse[i].id+'">'
					+ companyResponse[i].aadress  +'</td><td id="telid'+companyResponse[i].id+'">' 
					+ phoneFormatter("" + companyResponse[i].tel) +'</td><td id="emailid'+companyResponse[i].id+'">'
					+  companyResponse[i].email + '</td><th id="content" class="Hideable" style="display:block;"> <button id="' 
					+ companyResponse[i].id + '" class="editbtn btn btn-default">Muuda</button> <button class="btn btn-default modify-company" data-toggle="modal" data-target="#deleteCompanyModal"   onClick ="openDeleteCompanyConfirmModal('
					+ companyResponse[i].id + ')">Kustuta</button></th></tr>';
            }

            document.getElementById("CompanyTable").innerHTML = tableContent;
        }
    });
}

function intOnly(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && charCode != 43  && (charCode < 43 || charCode > 57)) {
        return false;	
    }else
    return true;
}
function phoneFormatter(numberToFormat){
	if(numberToFormat.length<=6){
		return numberToFormat.replace(/(\d\d\d)(\d\d\d)/, '$1-$2');
	}else if (numberToFormat.length<=7){
		return numberToFormat.replace(/(\d\d\d)(\d\d\d)(\d)/, '$1-$2-$3');
	}else if(numberToFormat.length<=8){
		return numberToFormat.replace(/(\d\d\d)(\d\d\d)(\d\d)/, '$1-$2-$3');
	}else if(numberToFormat.length<=9){
		return numberToFormat.replace(/(\d\d\d)(\d\d\d)(\d\d\d)/, '$1-$2-$3');
	}else{
		return numberToFormat.replace(/(\d\d\d)(\d\d\d)(\d\d\d)/, '$1-$2-$3');
	}
};
function addNewCompany(){
    var inputName = document.getElementById("newCompanyName").value;
    var inputTel = document.getElementById("newCompanyTel").value;
    var inputEmail = document.getElementById("newCompanyEmail").value;
    var inputContact = document.getElementById("newCompanyContact").value;
    var inputAddress = document.getElementById("newCompanyAddress").value;
    if((inputName != "")&& (inputTel != "")&& (inputEmail != "")&& (inputContact != "")&& (inputAddress != "")){
        var newCompany = JSON.stringify({"nimi": inputName,"kontaktisik": inputContact,"aadress": inputAddress,"tel": inputTel,"email": inputEmail});
        console.log(newCompany);
        $.ajax({
            async: true,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: newCompany,
            url: "http://localhost:3000/ettevotted",
            success: function(companyResponse){
                console.log(companyResponse);
                fillAllCompanyTable();
                showAlert("success-alert");
                location.reload()
            }
         });
    }else{
       // showAlert("danger-alert");
        $("#danger-alert").alert();
        $("#danger-alert").fadeTo(2000, 500);
    }
};

function showAlert(alertId){
    $("#"+alertId).alert();
    $("#"+alertId).fadeTo(2000, 500).slideUp(500, function(){
        $("#"+alertId).slideUp(500);
    });
}

function saveCompany(id){
	
	var inputValue = document.getElementById("firmaid"+id).innerHTML;
    var inputValue1 = document.getElementById("isikuid"+id).innerHTML;
    var inputValue2 = document.getElementById("aadressid"+id).innerHTML;
    var inputValue3 = document.getElementById("telid"+id).innerHTML;
    var inputValue4 = document.getElementById("emailid"+id).innerHTML;
    var modifiedCompany = JSON.stringify({"id": id,"nimi": inputValue,"kontaktisik": inputValue1, "aadress": inputValue2,"tel":inputValue3, "email":inputValue4});
    
        $.ajax({
            async: true,
            type: "PUT",
            dataType: "json",
            contentType: "application/json",
            data: modifiedCompany,
            url: "http://localhost:3000/ettevotted",
            success: function(companyResponse){
                fillAllCompanyTable();
                
            }
        });
}


var companyToDeleteId;
var companyToDeleteName;
var companyToDeleteTel;
var companyToDeleteEmail;
var companyToDeleteContact;
function openDeleteCompanyConfirmModal(selectedCompanyId){
    for(var i= 0; i<company.length; i++){
        if(company[i].id == selectedCompanyId){
            companyToDeleteId = selectedCompanyId;
            companyToDeleteName = company[i].nimi;
			companyToDeleteTel = company[i].tel;
			companyToDeleteEmail = company[i].email;
			companyToDeleteContact = company[i].kontakt;
            $(".modal-body #deleteMessage").text("Kas oled kindel, et soovid kustutada ettevõtte " + companyToDeleteName + "?");
        }
    }
}

function deleteCompany() {
   if(companyToDeleteId != null){
        $.ajax({
            async: true,
            type: "DELETE",
            dataType: "json",
            contentType: "application/json",
            url: "http://localhost:3000/ettevotted/" + companyToDeleteId,
            success: function(companyResponse){
                fillAllCompanyTable();
                $('#deleteCompanyModal').modal('hide');
                showAlert("delete-success-alert");
            }

        });
    }
    location.reload()
}
/*
function SwitchButtons(buttonId) {
	  var hideBtn, showBtn, menuToggle;
	  if (buttonId == 'button1') {
		menuToggle = 'menu2';
		showBtn = 'button2';
		hideBtn = 'button1';
	  } else {
		menuToggle = 'menu3';
		showBtn = 'button1';
		hideBtn = 'button2';
	  }
	  document.getElementById(hideBtn).style.display = 'none';
	  document.getElementById(showBtn).style.display = '';


}*/
function Hideable() {
    var peidaelement = document.getElementsByClassName('Hideable');
    if (peidaelement.style.display === "none") {
        peidaelement.style.display = "block";
    } else {
        peidaelement.style.display = "none";
    }
}

var rejectList = [ "deny.com" , "reject.net" ];

function validateEmailField()
{
var emailValue = $('#email-input').val(); // To Get Value (can use getElementById)
var splitArray = emailValue.split('@'); // To Get Array

if(rejectList.indexOf(splitArray[1]) >= 0)
{
// Means it has the rejected domains
return false;
}
return true;

}
