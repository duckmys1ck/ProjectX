var mysql      = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Parool11',
    database: 'haldusprogramm'
});
connection.connect(function(err){
if(!err) {
    console.log("Login andmebaas ühendatud...");
} else {
    console.log("Login andmebaas ei saa ühendust!");
}
});
/*
exports.register = function(req,res){
  // console.log("req",req.body);
  var today = new Date();
  var users={
    "first_name":req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email,
    "password":req.body.password,
    "created":today,
    "modified":today
  }
  connection.query('INSERT INTO users SET ?',users, function (error, results, fields) {
  if (error) {
    console.log("error ocurred",error);
    res.send({
      "code":400,
      "failed":"error ocurred"
    })
  }else{
    console.log('The solution is: ', results);
    res.send({
      "code":200,
      "success":"user registered sucessfully"
        });
  }
  });
}*/
exports.getUserByEmail = function(userEmail, callback) {
    connection.query('SELECT * FROM kasutajad WHERE kasutajanimi = ?',[userEmail], function (err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing query');
        }
})};

exports.login = function(req,res){
  var kasutajanimi= req.body.kasutajad_kasutajanimi;
  var parool = req.body.kasutajad_parool;
  connection.query('SELECT * FROM kasutajad WHERE kasutajanimi = ?',[kasutajanimi], function (error, results, fields) {
  if (error) {
    // console.log("error ocurred",error);
    res.send({
      "code":400,
      "failed":"error ocurred"
    })
  }else{
    console.log(results);
    if(results.length >0){
      if(results[0].parool == parool){
        var userId= results[0].id;
        console.log("userId = " + userId);
  		  connection.query('SELECT rollid.* FROM haldusprogramm.kasutajad INNER JOIN haldusprogramm.rollid on kasutajad.roll_id = rollid.id WHERE kasutajad.id = ?',[userId], function (errorRoles, resultsRoles, fieldsRoles) {
  			for(var roleIndex in resultsRoles) {
  				if(resultsRoles[roleIndex].roll == "Administraator") {
  					req.session.admin = true;
  					req.session.user = kasutajanimi;
  						res.send({
  							"role":"Administraator",
  							"userName":results[0].kasutajanimi,
  							"userId":results[0].id,
  							"code":200,
  							"success":"login sucessfull"
  							});
  				}
          if(resultsRoles[roleIndex].roll == "Ettevõtte peakasutaja") {
  					req.session.admin = true;
  					req.session.user = kasutajanimi;
  						res.send({
  							"role":"Ettevõtte peakasutaja",
  							"userName":results[0].kasutajanimi,
  							"userId":results[0].id,
  							"code":201,
  							"success":"login sucessfull"
  							});
  				}
          if(resultsRoles[roleIndex].roll == "Ettevõtte IT-Kasutaja") {
  					req.session.admin = true;
  					req.session.user = kasutajanimi;
  						res.send({
  							"role":"Ettevõtte IT-Kasutaja",
  							"userName":results[0].kasutajanimi,
  							"userId":results[0].id,
  							"code":202,
  							"success":"login sucessfull"
  							});
          if(resultsRoles[roleIndex].roll == "Töötaja") {
  					req.session.admin = true;
  					req.session.user = kasutajanimi;
  						res.send({
  							"role":"Töötaja",
  							"userName":results[0].kasutajanimi,
  							"userId":results[0].id,
  							"code":203,
  							"success":"login sucessfull"
              });
	        }
      			 }
           }
          });
        }else {
          res.send({
            "code":204,
            "success":"Email and password does not match"
              });
        }
      }else {
        res.send({
          "code":204,
          "success":"Email does not exits"
            });
      }
    }
    });
  }

  /* exports.passwordCheck = function(employee, callback){
    var id = employee.employee_id;
    var password = employee.employee_password;
    connection.query('SELECT employee_password FROM employee WHERE employee_id = ?',[id], function (error, results, fields) {
  	if(!error){
  		if(password == results[0].employee_password) {
  			return callback(results);
  		} else{
  			console.log("was wrong pw in loginroutes");
  			var wrongPasswordErr = new Error();
  			wrongPasswordErr.name = "WrongPasswordError";
  			wrongPasswordErr.message = "Old password incorrect!";
  			wrongPasswordErr.code = 205;
  			return callback(wrongPasswordErr);
  		}

  	} else {
  		console.log("error ocurred");
  	}
  })} */

  exports.logout = function (req, res) {
    req.session.destroy();
    res.send({
          "code":200,
          "success":"logout success!"
            });
  };
