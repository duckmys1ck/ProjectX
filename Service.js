var dbApis = require('./dbMysql');
var express = require('express');
var morgan = require('morgan');
var login = require('./routes/loginroutes');
var bodyParser = require('body-parser');
/*var fileUpload = require('express-fileupload');*/
var fs = require("fs");
var csv = require("csvtojson");
var session = require('express-session');

var hostname = 'localhost';
var port = 3000;

var app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
var router = express.Router();


// ETTEVÕTTED

app.all('/ettevotted', function(req, res, next){
    res.writeHead(200, {'Content-Type': 'application/json'});
    next();
});

app.get('/ametid', function(req, res, next){
    dbApis.getAllAmetid(function(ametidResponse){
        console.log(ametidResponse);
        res.end(JSON.stringify(ametidResponse));
    });
});

app.get('/ettevotted', function(req, res, next){
    dbApis.getAllCompany(function(companyResponse){
        console.log(companyResponse);
        res.end(JSON.stringify(companyResponse));
    });
});

app.post('/ettevotted', function(req, res, next){
    console.log(req.body);
    dbApis.addNewCompany(req.body.nimi,req.body.tel,req.body.email,req.body.kontaktisik,req.body.aadress, function(companyResponse){
        console.log(companyResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.put('/ettevotted', function(req, res, next){
    dbApis.updateCompany(req.body, function(companyResponse){
        console.log(companyResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.delete('/ettevotted/:companyId', function(req, res, next){
    dbApis.deleteCompanyById(req.params.companyId, function(companyResponse){
        console.log(companyResponse);
        res.end('{"id":'+ req.params.companyId + '}');
    });
});


// TÖÖTAJAD

app.all('/tootajad', function(req, res, next){
    res.writeHead(200, {'Content-Type': 'application/json'});
    next();
});

app.get('/tootajad', function(req, res, next){
    dbApis.getAllWorker(function(workerResponse){
        console.log(workerResponse);
        res.end(JSON.stringify(workerResponse));
    });
});

app.post('/tootajad', function(req, res, next){
    console.log(req.body);
    dbApis.addNewWorker(req.body.eesnimi,req.body.perekonnanimi,req.body.ameti_id,req.body.ettevotte_id,req.body.tel,req.body.email, function(WorkerResponse){
        console.log(WorkerResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.put('/tootajad', function(req, res, next){
    dbApis.updateWorker(req.body, function(WorkerResponse){
        console.log(WorkerResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.delete('/tootajad/:WorkerId', function(req, res, next){
    dbApis.deleteWorkerById(req.params.WorkerId, function(WorkerResponse){
        console.log(WorkerResponse);
        res.end('{"id":'+ req.params.WorkerId + '}');
    });
});


// KOOLITUSED

app.all('/koolitused', function(req, res, next){
    res.writeHead(200, {'Content-Type': 'application/json'});
    next();
});

app.get('/koolitused', function(req, res, next){
    dbApis.getAllKoolitus(function(KoolitusResponse){
        console.log(KoolitusResponse);
        res.end(JSON.stringify(KoolitusResponse));
    });
});

app.post('/koolitused', function(req, res, next){
    console.log(req.body);
    dbApis.addNewKoolitus(req.body.nimi,req.body.kirjeldus,req.body.tase,req.body.koolitustasu,req.body.kestus, function(KoolitusResponse){
        console.log(KoolitusResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.put('/koolitused', function(req, res, next){
    dbApis.saveKoolitus(req.body, function(KoolitusResponse){
        console.log(KoolitusResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.delete('/koolitused/:KoolitusId', function(req, res, next){
    dbApis.deleteKoolitusById(req.params.KoolitusId, function(KoolitusResponse){
        console.log(KoolitusResponse);
        res.end('{"id":'+ req.params.KoolitusId + '}');
    });
});


// MASSIMPORT

app.post('/tootajadFile', function(req, res, next){
  console.log(req.body);
  csv({
      delimiter: ';'
    })
     .fromString(req.body.message)
  .on('json', (worker)=>{
    console.log(worker);
      console.log(worker);
      dbApis.addNewWorker(worker.eesnimi, worker.perekonnanimi, worker.ameti_id, worker.ettevotte_id, worker.tel, worker.email, function(workersResponse){
          console.log(workersResponse);
          res.end(JSON.stringify(req.body));
      });
  })
    .on('done', (error) => {
    console.log('end csv to json');
    return res.status(500).send(error);
  })
  res.location('/static/tootajad.html');
  res.send(200, null);
});

app.use(express.static('/public'));
app.use('/static', express.static('public'))


// RIISTVARA + TARKVARA

app.get('/riistvara', function(req, res, next){
    dbApis.getAllRiistvara(function(RiistvaraResponse){
        console.log(RiistvaraResponse);
        res.end(JSON.stringify(RiistvaraResponse));
    });
});
app.get('/tarkvara', function(req, res, next){
    dbApis.getAllTarkvara(function(TarkvaraResponse){
        console.log(TarkvaraResponse);
        res.end(JSON.stringify(TarkvaraResponse));
    });
});

app.get('/recommendedtark', function(req, res, next){
    dbApis.getAllTarkvara(function(TarkvaraResponse){
        console.log(TarkvaraResponse);
        res.end(JSON.stringify(TarkvaraResponse));
    });
});
app.get('/recommendedriistvara', function(req, res, next){
    dbApis.getAllRecRiistvara(function(RecRiistvaraResponse){
        console.log(RecRiistvaraResponse);
        res.end(JSON.stringify(RecRiistvaraResponse));
    });
});

app.post('/riistvara', function(req, res, next){
    console.log(req.body);
    dbApis.addNewRiistvara(req.body.arvuti_nimetus,req.body.printeri_nimetus,req.body.monitori_nimetus,req.body.hiire_nimetus,req.body.klaviatuuri_nimetus,req.body.ruuteri_nimetus,req.body.kolari_nimetus,req.body.korvaklappide_nimetus,req.body.projektori_nimetus, function(addNewRiistvara){
        console.log(addNewRiistvara);
        res.end(JSON.stringify(req.body));
    });
});
app.post('/tarkvara', function(req, res, next){
    console.log(req.body);
    dbApis.addNewTarkvara(req.body.operatsioonisysteem,req.body.ms_office_versioon,req.body.antiviirus, function(addNewTarkvara){
        console.log(addNewTarkvara);
        res.end(JSON.stringify(req.body));
    });
});


// LOGIN

app.use(session({
    secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true
}));

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var authAdmin = function(req, res, next) {
    var user="";
    login.getUserByEmail(req.session.user, function(userResponse){
        if (userResponse.length == 1 && userResponse[0].kasutajanimi && req.session && req.session.admin) {
            return next();
        }else {
            return res.sendStatus(401);
        }
    });
};

var authCompanyUser = function(req, res, next) {
    var user="";
    login.getUserByEmail(req.session.user, function(userResponse){
        if (userResponse.length == 1 && userResponse[0].kasutajanimi && req.session && req.session.company || req.session.admin) {
            return next();
        }else {
            return res.sendStatus(401);
        }
    });
};

var authUser = function(req, res, next) {
    var user="";
    login.getUserByEmail(req.session.user, function(userResponse){
        if (userResponse.length == 1 && userResponse[0].kasutajanimi && req.session && req.session.user || req.session.company || req.session.admin) {
            return next();
        }else {
            return res.sendStatus(401);
        }
    });
};

app.get('/ettevotted.html', authAdmin);
app.get('/koolitused.html', authCompanyUser);
app.get('/tootajad.html', authCompanyUser);
app.get('/riistvara.html', authCompanyUser);

//router.post('/register',login.register);
router.post('/login',login.login);
router.post('/logout',login.logout);
app.use('/api', router);

router.get('/', function(req, res) {
    res.json({ message: 'welcome to our upload module apis' });
});

//router.post('/register',login.register);
router.post('/login',login.login);
app.use('/api', router);

app.use(express.static(__dirname + '/Public'));

// server listen
app.listen(port, hostname, function(){
    console.log(`Server is running at http://${hostname}:${port}/`)
});


/*
app.all('/koolitused', function(req, res, next){
    res.writeHead(200, {'Content-Type': 'application/json'});
    next();
});*/


/*
app.post('/koolitused', function(req, res, next){
    console.log(req.body);
    dbApis.addNewKoolitus(req.body.nimi,req.body.kirjeldus,req.body.tase,req.body.koolitustasu, function(KoolitusResponse){
        console.log(KoolitusResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.put('/koolitused', function(req, res, next){
    dbApis.updateKoolitus(req.body, function(KoolitusResponse){
        console.log(KoolitusResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.delete('/koolitused/:KoolitusId', function(req, res, next){
    dbApis.deleteKoolitusById(req.params.KoolitusId, function(KoolitusResponse){
        console.log(KoolitusResponse);
        res.end('{"id":'+ req.params.KoolitusId + '}');
    });
});
*/
