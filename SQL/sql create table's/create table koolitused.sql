create table koolitused(
id int(3) auto_increment primary key,
nimi varchar(100) not null,
kirjeldus varchar(200) not null, 
tase varchar(100) not null ,
koolitustasu int(4) not null
);