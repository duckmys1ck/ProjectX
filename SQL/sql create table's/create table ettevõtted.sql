CREATE TABLE ettevõtted (
id INT(3) NOT NULL AUTO_INCREMENT PRIMARY KEY,
nimi varchar(50) NOT NULL unique,
tel bigint(25) NOT NULL,
email varchar(50) NOT NULL,
kontaktisik varchar(25) NOT NULL
);