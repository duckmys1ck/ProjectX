create table tootajad(
id int(3) auto_increment primary key,
nimi varchar(20) not null unique,
tel int(25) not null,
email varchar(50) not null,
vanus int(3),
ametikoht varchar(20) not null
);