create table ettevõtted (
id INT(3) NOT NULL AUTO_INCREMENT PRIMARY KEY,
nimi varchar(50) NOT NULL unique,
tel bigint(25) NOT NULL,
email varchar(50) NOT NULL,
kontaktisik varchar(25) NOT NULL
);

create table ametid(
id int(2) not null primary key,
amet varchar(50) not null
);

create table kasutajad(
id int(3) auto_increment primary key,
roll_id int(3) not null,
kasutajanimi varchar(20) not null,
parool varchar(20) not null,
email varchar(50) not null,
tel varchar(25)
);

create table koolitused(
id int(3) auto_increment primary key,
nimi varchar(100) not null,
kirjeldus varchar(200) not null, 
tase varchar(100) not null ,
koolitustasu int(4) not null
);

create table rollid(
id int(1) not null primary key,
roll varchar(50) not null,
kirjeldus varchar(500) not null
);

create table tootajad(
id int(3) auto_increment primary key,
nimi varchar(20) not null unique,
tel int(25) not null,
email varchar(50) not null,
vanus int(3),
ametikoht varchar(20) not null
);