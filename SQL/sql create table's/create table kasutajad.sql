create table kasutajad(
id int(3) auto_increment primary key,
roll_id int(3) not null,
kasutajanimi varchar(20) not null,
parool varchar(20) not null,
email varchar(50) not null,
tel varchar(25)
);