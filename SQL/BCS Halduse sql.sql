CREATE DATABASE  IF NOT EXISTS `haldusprogramm` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `haldusprogramm`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: haldusprogramm
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ametid`
--

DROP TABLE IF EXISTS `ametid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ametid` (
  `id` int(2) NOT NULL,
  `amet` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ametid`
--

LOCK TABLES `ametid` WRITE;
/*!40000 ALTER TABLE `ametid` DISABLE KEYS */;
INSERT INTO `ametid` VALUES (1,'Töötaja');
/*!40000 ALTER TABLE `ametid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ettevotted`
--

DROP TABLE IF EXISTS `ettevotted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ettevotted` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nimi` varchar(50) NOT NULL,
  `tel` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `kontaktisik` varchar(25) NOT NULL,
  `aadress` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nimi` (`nimi`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ettevotted`
--

LOCK TABLES `ettevotted` WRITE;
/*!40000 ALTER TABLE `ettevotted` DISABLE KEYS */;
INSERT INTO `ettevotted` VALUES (177,'AS Kalev','534648525','info@kalev.ee','Kalev','Jüri');
/*!40000 ALTER TABLE `ettevotted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kasutajad`
--

DROP TABLE IF EXISTS `kasutajad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kasutajad` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `roll_id` int(3) NOT NULL,
  `kasutajanimi` varchar(50) NOT NULL,
  `parool` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tel` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_kasutajad_roll_id_idx` (`roll_id`),
  CONSTRAINT `FK_kasutajad_roll_id` FOREIGN KEY (`roll_id`) REFERENCES `rollid` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kasutajad`
--

LOCK TABLES `kasutajad` WRITE;
/*!40000 ALTER TABLE `kasutajad` DISABLE KEYS */;
INSERT INTO `kasutajad` VALUES (1,1,'test','test','test@test.test','564584548');
/*!40000 ALTER TABLE `kasutajad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `koolitused`
--

DROP TABLE IF EXISTS `koolitused`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `koolitused` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nimi` varchar(100) NOT NULL,
  `kirjeldus` varchar(200) NOT NULL,
  `tase` varchar(100) NOT NULL,
  `koolitustasu` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `koolitused`
--

LOCK TABLES `koolitused` WRITE;
/*!40000 ALTER TABLE `koolitused` DISABLE KEYS */;
INSERT INTO `koolitused` VALUES (1,'Arendaja','HTML / CSS / Javascript','Algaja',2500);
/*!40000 ALTER TABLE `koolitused` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rollid`
--

DROP TABLE IF EXISTS `rollid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rollid` (
  `id` int(1) NOT NULL,
  `roll` varchar(50) NOT NULL,
  `kirjeldus` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rollid`
--

LOCK TABLES `rollid` WRITE;
/*!40000 ALTER TABLE `rollid` DISABLE KEYS */;
INSERT INTO `rollid` VALUES (1,'Administraator','BCS - Kõikide õigustega kasutaja'),(2,'Ettevõtte peakasutaja','IT infrastruktuuri tarkvarade haldamine / Töötajate haldamine oma ettevõttes / Personali oskuste haldamine'),(3,'Ettevõtte IT-kasutaja','IT infrastruktuuri tarkvarade haldamine'),(4,'Töötaja','Töötaja enesehinnangu lehe/testi täitmine ja salvestamine töötaja poolt');
/*!40000 ALTER TABLE `rollid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tootajad`
--

DROP TABLE IF EXISTS `tootajad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tootajad` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `eesnimi` varchar(20) NOT NULL,
  `perekonnanimi` varchar(20) NOT NULL,
  `ameti_id` int(3) NOT NULL,
  `vanus` int(3) NOT NULL,
  `tel` int(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nimi` (`eesnimi`),
  KEY `FK_tootajad_ameti_id_idx` (`ameti_id`),
  CONSTRAINT `FK_tootajad_ameti_id` FOREIGN KEY (`ameti_id`) REFERENCES `ametid` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tootajad`
--

LOCK TABLES `tootajad` WRITE;
/*!40000 ALTER TABLE `tootajad` DISABLE KEYS */;
INSERT INTO `tootajad` VALUES (1,'Juku','Kalle',1,22,53464855,'juku.kalle@gmail.com');
/*!40000 ALTER TABLE `tootajad` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-06 15:11:05
