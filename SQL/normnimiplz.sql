-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: haldusprogramm
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ametid`
--

DROP TABLE IF EXISTS `ametid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ametid` (
  `id` int(2) NOT NULL,
  `amet` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ametid`
--

LOCK TABLES `ametid` WRITE;
/*!40000 ALTER TABLE `ametid` DISABLE KEYS */;
INSERT INTO `ametid` VALUES (1,'Töötaja'),(2,'Sekretär'),(3,'Juhataja');
/*!40000 ALTER TABLE `ametid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ettevotted`
--

DROP TABLE IF EXISTS `ettevotted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ettevotted` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nimi` varchar(50) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `kontaktisik` varchar(25) NOT NULL,
  `aadress` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ettevotted`
--

LOCK TABLES `ettevotted` WRITE;
/*!40000 ALTER TABLE `ettevotted` DISABLE KEYS */;
INSERT INTO `ettevotted` VALUES (200,'roos','55885','sasa','soos','koos'),(201,'sdafds','3425','fadfadsf','vasdvav','dfavsd'),(202,'siajcsia','584812','sfainas','ASDJIVAI','sfjaifjiasfji');
/*!40000 ALTER TABLE `ettevotted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kasutajad`
--

DROP TABLE IF EXISTS `kasutajad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kasutajad` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `roll_id` int(3) NOT NULL,
  `kasutajanimi` varchar(50) NOT NULL,
  `parool` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tel` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_kasutajad_roll_id_idx` (`roll_id`),
  CONSTRAINT `FK_kasutajad_roll_id` FOREIGN KEY (`roll_id`) REFERENCES `rollid` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kasutajad`
--

LOCK TABLES `kasutajad` WRITE;
/*!40000 ALTER TABLE `kasutajad` DISABLE KEYS */;
INSERT INTO `kasutajad` VALUES (1,1,'test','test','test@test.test','564584548');
/*!40000 ALTER TABLE `kasutajad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `koolitused`
--

DROP TABLE IF EXISTS `koolitused`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `koolitused` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nimi` varchar(100) NOT NULL,
  `kirjeldus` text NOT NULL,
  `tase` varchar(50) NOT NULL,
  `koolitustasu` decimal(6,2) NOT NULL,
  `kestus` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `koolitused`
--

LOCK TABLES `koolitused` WRITE;
/*!40000 ALTER TABLE `koolitused` DISABLE KEYS */;
INSERT INTO `koolitused` VALUES (5,'roos','soos','fa1',20.22,40),(6,'dasdsasd1','asdasdad','a',25.41,54);
/*!40000 ALTER TABLE `koolitused` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `new_view`
--

DROP TABLE IF EXISTS `new_view`;
/*!50001 DROP VIEW IF EXISTS `new_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `new_view` AS SELECT 
 1 AS `kasutajanimi`,
 1 AS `roll_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `riistidettevotteid`
--

DROP TABLE IF EXISTS `riistidettevotteid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riistidettevotteid` (
  `ettevotte_id` int(3) NOT NULL,
  `riistvara_id` int(3) NOT NULL,
  `id` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Ettevotte_idx` (`ettevotte_id`),
  KEY `FK_Riistvara_idx` (`riistvara_id`),
  CONSTRAINT `FK_Ettevotte` FOREIGN KEY (`ettevotte_id`) REFERENCES `ettevotted` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Riistvara` FOREIGN KEY (`riistvara_id`) REFERENCES `riistvara` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `riistidettevotteid`
--

LOCK TABLES `riistidettevotteid` WRITE;
/*!40000 ALTER TABLE `riistidettevotteid` DISABLE KEYS */;
/*!40000 ALTER TABLE `riistidettevotteid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `riistvara`
--

DROP TABLE IF EXISTS `riistvara`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riistvara` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `arvuti_nimetus` varchar(50) DEFAULT NULL,
  `printeri_nimetus` varchar(50) DEFAULT NULL,
  `monitori_nimetus` varchar(50) DEFAULT NULL,
  `hiire_nimetus` varchar(50) DEFAULT NULL,
  `klaviatuuri_nimetus` varchar(50) DEFAULT NULL,
  `ruuteri_nimetus` varchar(50) DEFAULT NULL,
  `kolari_nimetus` varchar(50) DEFAULT NULL,
  `korvaklappide_nimetus` varchar(50) DEFAULT NULL,
  `projektori_nimetus` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `riistvara`
--

LOCK TABLES `riistvara` WRITE;
/*!40000 ALTER TABLE `riistvara` DISABLE KEYS */;
INSERT INTO `riistvara` VALUES (0,'Samsung Pro','Canon pixma','X22PG29 Xiomi','Logitech M100','Logitech k150','Enigma 15S','Logitech s50','kingston hyperX','Samsung x1234'),(1,'Macbooc pro','Pixmel prisma','S27Myiagu','Logitech g920','Logitech k250','Router 228','HARDBASSONLY','ei ole ','opa4ki'),(5,'ITS','MY','LIFE','GONNA','DO','WHAT','I','DO','ITSMYLIFE'),(6,'its','my','life','gonna','do','what','i','do','itsmylife'),(7,'','','','','','','','','');
/*!40000 ALTER TABLE `riistvara` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rollid`
--

DROP TABLE IF EXISTS `rollid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rollid` (
  `id` int(1) NOT NULL,
  `roll` varchar(50) NOT NULL,
  `kirjeldus` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rollid`
--

LOCK TABLES `rollid` WRITE;
/*!40000 ALTER TABLE `rollid` DISABLE KEYS */;
INSERT INTO `rollid` VALUES (1,'Administraator','BCS - Kõikide õigustega kasutaja'),(2,'Ettevõtte peakasutaja','IT infrastruktuuri tarkvarade haldamine / Töötajate haldamine oma ettevõttes / Personali oskuste haldamine'),(3,'Ettevõtte IT-kasutaja','IT infrastruktuuri tarkvarade haldamine'),(4,'Töötaja','Töötaja enesehinnangu lehe/testi täitmine ja salvestamine töötaja poolt');
/*!40000 ALTER TABLE `rollid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tarkvara`
--

DROP TABLE IF EXISTS `tarkvara`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarkvara` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `operatsioonisysteem` varchar(50) DEFAULT NULL,
  `ms_office_versioon` varchar(50) DEFAULT NULL,
  `antiviirus` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tarkvara`
--

LOCK TABLES `tarkvara` WRITE;
/*!40000 ALTER TABLE `tarkvara` DISABLE KEYS */;
INSERT INTO `tarkvara` VALUES (0,'Windows 10','2017','MLG'),(1,'windows xp','1998','mlg404'),(2,'windows vista','1441','avast'),(3,'windows glista','142215','f secure');
/*!40000 ALTER TABLE `tarkvara` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tootajad`
--

DROP TABLE IF EXISTS `tootajad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tootajad` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `eesnimi` varchar(20) NOT NULL,
  `perekonnanimi` varchar(20) NOT NULL,
  `ameti_id` int(3) NOT NULL,
  `ettevotte_id` int(3) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tootajad_ameti_id_idx` (`ameti_id`),
  KEY `FK_tootajad_ettevotted_id_idx` (`ettevotte_id`),
  CONSTRAINT `FK_tootajad_ameti_id` FOREIGN KEY (`ameti_id`) REFERENCES `ametid` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_tootajad_ettevotte_id` FOREIGN KEY (`ettevotte_id`) REFERENCES `ettevotted` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tootajad`
--

LOCK TABLES `tootajad` WRITE;
/*!40000 ALTER TABLE `tootajad` DISABLE KEYS */;
INSERT INTO `tootajad` VALUES (98,'Rein','Tool',1,200,'555555','elu@elu.ee'),(99,'Siim','Saal',1,200,'5679892','elu@elu.ee'),(100,'Tiit','Kallas',1,200,'578990','elu@elu.ee'),(101,'Elina','Reinoja',1,200,'5612288','elu@elu.ee'),(130,'Lol','kek',2,200,'55523525','523');
/*!40000 ALTER TABLE `tootajad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `vaade2`
--

DROP TABLE IF EXISTS `vaade2`;
/*!50001 DROP VIEW IF EXISTS `vaade2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vaade2` AS SELECT 
 1 AS `kasutajanimi`,
 1 AS `roll_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `new_view`
--

/*!50001 DROP VIEW IF EXISTS `new_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `new_view` AS select `kasutajad`.`kasutajanimi` AS `kasutajanimi`,`kasutajad`.`roll_id` AS `roll_id` from `kasutajad` where (`kasutajad`.`roll_id` = '4') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vaade2`
--

/*!50001 DROP VIEW IF EXISTS `vaade2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vaade2` AS select `kasutajad`.`kasutajanimi` AS `kasutajanimi`,`kasutajad`.`roll_id` AS `roll_id` from `kasutajad` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-22 10:26:51
